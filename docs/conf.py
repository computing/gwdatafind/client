# gwdatafind documentation build configuration file

import inspect
import re
import sys
from os import getenv
from pathlib import Path

import gwdatafind

# -- metadata

project = "gwdatafind"
copyright = "2018-2025, Cardiff University"
author = "Duncan Macleod"
release = gwdatafind.__version__
version = re.split(r'[\w-]', gwdatafind.__version__)[0]

# -- config

source_suffix = '.rst'
master_doc = 'index'

default_role = 'obj'

# -- theme

html_theme = "furo"

html_theme_options = {
    "footer_icons": [
        {
            "name": "GitLab",
            "url": "https://git.ligo.org/computing/gwdatafind/client",
            "class": "fa-brands fa-gitlab",
        },
    ],
}

# need fontawesome for the gitlab icon in the footer
html_css_files = [
    "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/fontawesome.min.css",
    "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/solid.min.css",
    "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/brands.min.css",
]

pygments_dark_style = "monokai"

# -- extensions

extensions = [
    "sphinx.ext.intersphinx",
    "sphinx.ext.napoleon",
    "sphinx.ext.linkcode",
    "sphinx_automodapi.automodapi",
    "sphinx_copybutton",
    "sphinxarg.ext",
]

# automodapi
automodapi_inherited_members = False

# intersphinx
intersphinx_mapping = {
    "igwn-auth-utils": (
        "https://igwn-auth-utils.readthedocs.io/en/stable/",
        None,
    ),
    "igwn-segments": (
        "https://igwn-segments.readthedocs.io/en/stable/",
        None,
    ),
    "python": (
        "https://docs.python.org/",
        None,
    ),
    "requests": (
        "https://requests.readthedocs.io/en/stable/",
        None,
    ),
    "scitokens": (
        "https://scitokens.readthedocs.io/en/stable/",
        None,
    ),
}

# napoleon
napoleon_use_rtype = False


# -- linkcode

def _project_git_ref(version, prefix="v"):
    """Returns the git reference for the given full release version.
    """
    # handle builds in CI
    if getenv("GITLAB_CI"):
        return getenv("CI_COMMIT_REF")
    if getenv("GITHUB_ACTIONS"):
        return getenv("GITHUB_SHA")
    # otherwise use the project metadata
    _setuptools_scm_version_regex = re.compile(
        r"\+g(\w+)(?:\Z|\.)",
    )
    if match := _setuptools_scm_version_regex.search(version):
        return match.groups()[0]
    return f"{prefix}{version}"


PROJECT_GIT_REF = _project_git_ref(release, prefix="")
PROJECT_PATH = Path(gwdatafind.__file__).parent
PROJECT_URL = getenv(
    "CI_PROJECT_URL",
    "https://git.ligo.org/computing/gwdatafind/client",
)
PROJECT_BLOB_URL = f"{PROJECT_URL}/blob/{PROJECT_GIT_REF}/{PROJECT_PATH.name}"


def linkcode_resolve(domain, info):
    """Determine the URL corresponding to Python object.
    """
    if domain != "py" or not info["module"]:
        return None

    def find_source(module, fullname):
        """Construct a source file reference for an object reference.
        """
        # resolve object
        obj = sys.modules[module]
        for part in fullname.split("."):
            obj = getattr(obj, part)
        # get filename relative to project
        filename = Path(
            inspect.getsourcefile(obj),  # type: ignore [arg-type]
        ).relative_to(PROJECT_PATH).as_posix()
        # get line numbers of this object
        lines, lineno = inspect.findsource(obj)
        if lineno:
            start = lineno + 1  # 0-index
            end = lineno + len(inspect.getblock(lines[lineno:]))
        else:
            start = end = 0
        return filename, start, end

    try:
        path, start, end = find_source(info["module"], info["fullname"])
    except (
        AttributeError,  # object not found
        OSError,  # file not found
        TypeError,  # source for object not found
        ValueError,  # file not from this project
    ):
        return None

    url = f"{PROJECT_BLOB_URL}/{path}"
    if start:
        url += f"#L{start}-L{end}"
    return url
