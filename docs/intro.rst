.. _gwdatafind-introduction:

###########
Basic usage
###########

The GWDataFind service allows users to query for the location of
Gravitational-Wave data files containing data associated with
gravitational-wave detectors.

This package provides a number of functions to make requests to
a GWDataFind server with authorization credential handling.

.. _gwdatafind-top-api:

=============
Top-level API
=============

The :mod:`gwdatafind` top-level module provides a number of convenience
functions to perform single queries without a persistent connection:

.. currentmodule:: gwdatafind

.. autosummary::
    :nosignatures:

    ping
    find_observatories
    find_types
    find_times
    find_url
    find_urls
    find_latest

For example:

.. code-block:: python
    :caption: A simple query for URLs associated with Open Data
    :name: gwdatafind-basic-find_urls

    >>> from gwdatafind import find_urls
    >>> urls = find_urls(
    ...     "L",
    ...     "L1_GWOSC_O2_4KHZ_R1",
    ...     1187008880,
    ...     1187008884,
    ...     host="datafind.gwosc.org",
    ... )
    >>> print(urls)
    ['file://localhost/cvmfs/gwosc.osgstorage.org/gwdata/O2/strain.4k/frame.v1/L1/1186988032/L-L1_GWOSC_O2_4KHZ_R1-1187008512-4096.gwf']

The above examples work well for 'one-shot' queries where only a single
result is required.
For repeated queries, or similar, that require multiple connections to the
same GWDataFind host, see :doc:`session`.

.. _gwdatafind-cli:

======================
Command-line interface
======================

GWDataFind also provides a command-line interface accessible via
module execution (``python -m gwdatafind``) or the ``gw_data_find``
entry point script:

.. argparse::
    :module: gwdatafind.__main__
    :func: command_line
    :prog: gw_data_find
    :nodescription:
