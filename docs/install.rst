.. _install:

#####################
Installing GWDataFind
#####################

.. _conda:

=====
Conda
=====

The recommended method of installing GWDataFind is with
`Conda <https://conda.io>`__
(or `Mamba <https://mamba.readthedocs.io/>`__):

.. code-block:: bash
    :name: install-conda
    :caption: Installing gwdatafind with conda

    conda install -c conda-forge gwdatafind

.. _debian:

======
Debian
======

.. code-block:: bash
    :name: install-debian
    :caption: Installing gwdatafind with Apt

    apt-get install gwdatafind

See the IGWN Computing Guide software repositories entry for
`Debian <https://computing.docs.ligo.org/guide/software/debian/>`__
for instructions on how to configure the required
IGWN Debian repositories.

To install the Python 3 library only (and not any command-line entry points):

.. code-block:: bash
    :name: install-debian-library
    :caption: Installing python3-gwdatafind with Apt

    apt-get install python3-gwdatafind

.. _pip:

===
Pip
===

.. code-block:: bash
    :name: install-pip
    :caption: Installing gwdatafind with Pip

    python -m pip install gwdatafind

.. _el:

==========================================
RedHat / CentOS / Scientific / Rocky Linux
==========================================

.. code-block:: bash
    :name: install-el
    :caption: Installing gwdatafind with DNF

    dnf install gwdatafind

See the IGWN Computing Guide software repositories entries for
`Scientific Linux 7
<https://computing.docs.ligo.org/guide/software/sl7/>`__
or
`Rocky Linux 8 <https://computing.docs.ligo.org/guide/software/rl8/>`__
for instructions on how to configure the required IGWN Yum repositories.

To install the Python 3 library only (and not any command-line entry points):

.. code-block:: bash
    :name: install-el-library
    :caption: Installing python3-gwdatafind with DNF

    dnf install python3-gwdatafind
