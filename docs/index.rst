
##########
GWDataFind
##########

.. toctree::
    :hidden:

    GWDatafind <self>

The client library for the GWDataFind service.

.. toctree::
    :caption: Documentation
    :maxdepth: 1

    Installation <install>
    Basic usage <intro>
    Session usage <session>
    Authorisation <auth>

.. toctree::
    :caption: Reference
    :maxdepth: 2

    api
