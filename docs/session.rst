.. _gwdatafind-session:

#############
Session usage
#############

All of the functions introduced in the :ref:`gwdatafind-top-api` accept
a ``session`` keyword to enable reusing a connection to a GWDataFind
host.

For example:

.. code-block:: python
    :caption: Example use of a `gwdatafind.Session` to reuse a connection.
    :name: gwdatafind-session-example

    >>> from gwdatafind import (
    ...     find_observatories,
    ...     find_urls,
    ...     Session,
    ... )
    >>> with Session() as sess:
    ...     obs = find_observatories(
    ...         host="datafind.gwosc.org",
    ...         session=sess,
    ...     )
    ...     print(obs)
    ...     urls = {}
    ...     for ifo in obs:
    ...         urls[ifo] = find_urls(
    ...             ifo,
    ...             f"{ifo}1_GWOSC_O2_4KHZ_R1",
    ...             1187008880,
    ...             1187008884,
    ...             host="datafind.gwosc.org",
    ...             session=sess,
    ...         )
    ...     print(urls)
    ['H', 'V', 'L']
    {'H': ['file://localhost/cvmfs/gwosc.osgstorage.org/gwdata/O2/strain.4k/frame.v1/H1/1186988032/H-H1_GWOSC_O2_4KHZ_R1-1187008512-4096.gwf'],
     'V': ['file://localhost/cvmfs/gwosc.osgstorage.org/gwdata/O2/strain.4k/frame.v1/V1/1186988032/V-V1_GWOSC_O2_4KHZ_R1-1187008512-4096.gwf'],
     'L': ['file://localhost/cvmfs/gwosc.osgstorage.org/gwdata/O2/strain.4k/frame.v1/L1/1186988032/L-L1_GWOSC_O2_4KHZ_R1-1187008512-4096.gwf']}

In the above example the connection to ``datafind.gwosc.org`` is
held open and reused to simplify subsequent queries and minimise the risk
of network communication issues.

The `gwdatafind.Session` object is just an import of
:external+igwn-auth-utils:py:class:`igwn_auth_utils.Session`,
a wrapper around :external+requests:py:class:`requests.Session` that
automatically handles IGWN authorisation credentials/tokens.
For more details on credential or token usage, see :doc:`auth`.
